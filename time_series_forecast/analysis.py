import numpy
import matplotlib.pyplot as plt

def plot_results(dataset: numpy.ndarray,
              look_back: int,
              train_predict: numpy.ndarray,
              test_predict: numpy.ndarray,
              forecast_predict: numpy.ndarray
              ):
    """
    Plots baseline and predictions.
    blue: baseline
    green: prediction with training data
    red: prediction with test data
    cyan: prediction based on predictions
    :param dataset: dataset used for predictions
    :param look_back: number of previous time steps as int
    :param train_predict: predicted values based on training data
    :param test_predict: predicted values based on test data
    :param forecast_predict: predicted values based on previous predictions
    :return: None
    """
    plt.plot(dataset, label='expected values')
    plt.plot([None for _ in range(look_back)]
            +[x for x in train_predict]
            , label='training set predicted values')
    plt.plot([None for _ in range(look_back)]
            +[None for _ in train_predict]
            +[x for x in test_predict]
            , label='test set predicted values')
    if (len(forecast_predict)>0):
        plt.plot([None for _ in range(look_back)]
                + [None for _ in train_predict]
                + [x for x in forecast_predict]
                , label='test set forecast values'
                )
    plt.legend()
    plt.show()
