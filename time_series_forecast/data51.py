import numpy
import pandas
from pandas import read_csv
from pandas import DataFrame
from pandas import concat
from pandas import Series
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from numpy import concatenate

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from math import sqrt
from tqdm import trange
from matplotlib import pyplot as plt

from time_series_forecast.analysis import plot_results

numpy.random.seed(13) #fixed random seed for reproductibility

DATA_FILE = './data/API_MAR_DS2_en_csv_v2.csv'


def features_pick(df: DataFrame, feature_name: str, n_features_max: int=10) -> list:
    """
	Naive way to look for highly correlated features
    that are not inter-correlated
    """
    abs_corr = df.corr().abs()
    selected_features = [abs_corr[feature_name][lambda x: (x > .6) & (x< 1)].idxmax()]
    hight_abs_corr = df[abs_corr[feature_name][lambda x: x > .7].index].corr().abs()
    while not len(selected_features) >= n_features_max - 1 :
    	selected_features.append(hight_abs_corr[selected_features].drop(selected_features).min(axis=1).idxmin())
    return [feature_name] + selected_features


def split_dataset(dataset: numpy.ndarray, train_size, look_back) -> (numpy.ndarray, numpy.ndarray):
    """
    Splits dataset into training and test datasets. The last `look_back` rows in train dataset
    will be used as `look_back` for the test dataset.
    :param dataset: source dataset
    :param train_size: specifies the train data size
    :param look_back: number of previous time steps as int
    :return: tuple of training data and test dataset
    """
    if not train_size > look_back:
        raise ValueError('train_size must be lager than look_back')
    train, test = dataset[0:train_size, :], dataset[train_size - look_back:len(dataset), :]
    print('train_dataset: {}, test_dataset: {}'.format(len(train), len(test)))
    return train, test


def create_dataset(dataset: numpy.ndarray, look_back: int=1, nb_features: int=1) -> (numpy.ndarray, numpy.ndarray):
    """
    The function takes two arguments: the `dataset`, which is a NumPy array that we want to convert into a dataset,
    and the `look_back`, which is the number of previous time steps to use as input variables
    to predict the next time period.
    :param dataset: numpy dataset
    :param look_back: number of previous time steps as int
    :return: tuple of input and output dataset
    """
    data_x, data_y = [], []
    for i in range(len(dataset)-look_back-1):
        data_x.append(dataset[i:(i+look_back), 0:nb_features]) #datax shape should be [samples, time steps, features]
        data_y.append(dataset[i + look_back, 0]) #datay shape should be [samples, unit] where unit =1 here as we only want to predict one feature (the first one in our dataset)
    return numpy.array(data_x), numpy.array(data_y)


def build_model(look_back: int, batch_size: int=1, nb_features: int=1,
                nb_neurons: int=50, activation: str='relu',
                stateful: bin=False) -> Sequential:
    """
    The function builds a keras Sequential model
    :param look_back: number of previous time steps as int
    :param batch_size: batch_size as int, defaults to 1
    :return: keras Sequential model
    """
    model = Sequential()
    if stateful:
        model.add(LSTM(nb_neurons,
                       activation=activation, #'tanh', 'linear'
                       input_shape=(look_back, nb_features),
                       batch_input_shape=(batch_size, look_back, nb_features),
                       stateful=True,
                       return_sequences=False))
    else:
        model.add(LSTM(nb_neurons,
                       activation=activation, #'tanh', 'linear'
                       input_shape=(look_back, nb_features),
                       return_sequences=False))
    model.add(Dense(1, activation='linear'))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def make_forecast_predictions(model: Sequential, test_x: numpy.ndarray, batch_size: int=1):
    forecast_predict = numpy.empty((0, 1), dtype=numpy.float32)
    for i in range(len(test_x)):
        # extract sample i from test_x and replace known values of feature 0 with forecast predictions
        # (and keep real data for linked features as we don't predict them)
        look_back_buffer = test_x[i:(i+1),:,:].copy()
        nb_steps = look_back_buffer.shape[1]
        nb_step_preds = min(len(forecast_predict), nb_steps)
        for si in range(nb_step_preds):
            look_back_buffer[0, nb_steps-si-1, 0] = forecast_predict[len(forecast_predict)-si-1]
        # make prediction with current lookback buffer
        cur_predict = model.predict(look_back_buffer, batch_size)
        # add prediction to forecast predictions list
        forecast_predict = numpy.concatenate([forecast_predict, cur_predict], axis=0)
    return forecast_predict


def normalize(df: DataFrame) -> DataFrame:
    result = df.copy()
    for feature_name in df.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result


def normalize2(df: DataFrame) -> (MinMaxScaler, DataFrame):
    # normalize the dataset
    df = df.fillna(0)
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataset = scaler.fit_transform(dataset)

    return scaler, dataset

def normalize_and_clean(df: DataFrame, target_name: str) -> numpy.ndarray:
    dataset = normalize(df).dropna(subset=[target_name]).fillna(0).values
    return dataset

def rescale(vals, min, max):
	result = vals.copy()
	for i in range(len(vals)):
		result[i] = vals[i]*(max-min)+min
	return result


def main(target_name: str, max_linked_features=25 , fraction_keep_for_test=0.25,
        look_back=1, batch_size=1, nb_epochs=50,
        nb_neurons: int=50, activation: str='relu', stateful: bin=False):
    """
        Parameters:
        target_name Name of the feature to make a predictor for
        max_linked_features: Set to 0 to make a univariate analysis
        fraction_keep_for_test: fraction of the dataset to keep for testing
        look_back: look_back
    """

    # load data
    raw_data = read_csv(DATA_FILE, skiprows=4)

    # Make a correspondance table
    indicator_meaning = raw_data[["Indicator Name", "Indicator Code"]]
    raw_data = raw_data.drop(["Country Name", "Country Code", "Indicator Name", "Unnamed: 62"], axis=1)
    # transpose dataframe to get years as index
    raw_data = raw_data.set_index("Indicator Code").transpose()
    # Drop duplicates columns (like time to start a business male/female)
    raw_data = raw_data.T.drop_duplicates().T
    # Remove constant columns (ignoring NaNs)
    raw_data = raw_data.loc[:,raw_data.apply(Series.nunique) != 1]
    # Just look at time series with more than 40 values
    #raw_data = raw_data.loc[:,raw_data.apply(Series.nunique) > 40]

    #Extract time series of this feature + associated features that may be usefull to predict it
    sel_features = features_pick(raw_data, target_name, max_linked_features)
    fdataset = raw_data[sel_features]
    #print(fdataset)

    #normalize and remove NA rows
    dataset = normalize_and_clean(fdataset, target_name)
    #print(dataset)

    #split into train and test sets
    train_size = max(1, int(len(dataset) * (1-fraction_keep_for_test)))
    train, test = split_dataset(dataset, train_size, look_back)
    print("dataset_size: ",len(dataset))
    print("look_back: ",look_back)
    print("train_size: ",train_size)

    #reshape into X=t and Y=t+1
    train_x, train_y = create_dataset(train, look_back, len(sel_features))
    test_x, test_y = create_dataset(test, look_back, len(sel_features))

    print("train x:",train_x.shape)
    print("train y:",train_y.shape)
    print("test x:",test_x.shape)
    print("test y:",test_y.shape)

    #create and fit Multilayer Perceptron model
    model = build_model(look_back, batch_size, len(sel_features))

    if stateful:
        # Autre façon d'entrainer qui change resultats si LSTM model defini come stateful=True.
        # sinon ne change rien par rapport a methode ci-dessous ou state non reseté entre chaque epoch
        for _ in range(nb_epochs):
            model.fit(train_x, train_y, epochs=1, batch_size=batch_size, verbose=0, shuffle=False)
            model.reset_states()
    else:
        history = model.fit(train_x, train_y, epochs=nb_epochs, batch_size=batch_size,
                            validation_data=(test_x, test_y), verbose=0, shuffle=False)

        # plot history
        plt.plot(history.history['loss'], label='train')
        plt.plot(history.history['val_loss'], label='test')
        plt.legend()
        plt.show()


    #generate predictions for training set (to see if the model is correctly able to predict dataset it was trained on)
    train_predict = model.predict(train_x, batch_size)
    #genereate predictions for test set
    test_predict = model.predict(test_x, batch_size)
    print("test_predict: ",  test_predict.shape)
    #generate forecast predictions
    forecast_predict = []
    if (max_linked_features==0): #if univariate analysis. Else forecast prediction do not really make sense
        forecast_predict = make_forecast_predictions(model, test_x, batch_size=batch_size)

    #rescale dataset and predictions
    mindv = raw_data[target_name].min()
    maxdv = raw_data[target_name].max()
    dataset = rescale(dataset[:,0], mindv, maxdv)
    train_predict = rescale(train_predict, mindv, maxdv)
    train_y = rescale(train_y, mindv, maxdv)
    test_predict = rescale(test_predict, mindv, maxdv)
    test_y = rescale(test_y, mindv, maxdv)
    forecast_predict = rescale(forecast_predict, mindv, maxdv)

    #calculate root mean squared error
    train_score = numpy.sqrt(mean_squared_error(train_y, train_predict))
    print('Train Score: %.2f RMSE' % train_score)
    test_score = numpy.sqrt(mean_squared_error(test_y, test_predict))
    print('Test Score: %.2f RMSE' % test_score)

    #display results
    plot_results(dataset, look_back, train_predict, test_predict, forecast_predict)


if __name__ == "__main__":
    target_name = 'SG.GEN.PARL.ZS'
    main(target_name)
