#!/usr/bin/env python

from glob import glob
from setuptools import setup


def main():
    setup(
        author='Matthieu Bernard',
        author_email='matth.bernard@gmail.com',
        data_files=[('etc', glob('etc/*'))],
        description='Time series forecasting',
        long_description='Test time series forecasting with keras',
        maintainer='Matthieu Bernard',
        maintainer_email='matth.bernard@gmail.com',
        name='time_series_forecast',
        packages=['time_series_forecast'],
        scripts=[],
        url='https://gitlab.com/matthieu-bernard/test_neural_network',
        # Version is managed by conda in meta.yaml so no version...
        zip_safe=False
    )


if __name__ == '__main__':
    main()
